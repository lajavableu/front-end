import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../../components';

const routes: Routes = [

    /* // == Route exemple == */
    // {
    //     path: 'login',
    //     component: LoginComponent, // Component use in router-outlet
    //     canActivate: [UnauthentGuard] // guard that determine if the component can be loaded
    // },

    // ======================================================
    // == Home page ==
    {
        path: 'home',
        component: HomeComponent, // Component use in router-outlet
        pathMatch: 'full'
    },

    // == Root page ==
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },

    // == All other routes redirect ==
    {
        path: '**',
        redirectTo: '/home'
    }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }
